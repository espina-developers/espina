/*
    Copyright (c) 2013, Jorge Peña Pastor <jpena@cesvima.upm.es>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
        * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
        * Neither the name of the <organization> nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY Jorge Peña Pastor <jpena@cesvima.upm.es> ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL Jorge Peña Pastor <jpena@cesvima.upm.es> BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// ESPINA
#include "Output.h"
#include "Filter.h"
#include "DataProxy.h"
#include "Analysis.h"
#include "Segmentation.h"

// VTK
#include <vtkMath.h>

using namespace ESPINA;

const int ESPINA::Output::INVALID_OUTPUT_ID = -1;

TimeStamp Output::s_tick = 0;

//----------------------------------------------------------------------------
Output::Output(FilterPtr filter, const Output::Id& id, const NmVector3 &spacing)
: m_filter{filter}
, m_id{id}
, m_spacing{spacing}
, m_timeStamp{s_tick++}
{
}

//----------------------------------------------------------------------------
Output::~Output()
{
}

//----------------------------------------------------------------------------
void Output::setSpacing(const NmVector3& spacing)
{
  if (m_spacing != spacing)
  {
    m_spacing = spacing;

    for(auto data : m_data)
    {
      if (data->isValid())
      {
        data->setSpacing(spacing);
      }
    }

    updateModificationTime();
  }
}

//----------------------------------------------------------------------------
NmVector3 Output::spacing() const
{
  return m_spacing;
//   NmVector3 result = m_spacing;
//
//   if (result == NmVector3{0,0,0})
//   {
//     if (m_data.isEmpty()) throw Invalid_Bounds_Exception();
//
//     result = m_data[m_data.keys().first()]->spacing();
//   }
//
//  return result;
}

//----------------------------------------------------------------------------
Snapshot Output::snapshot(TemporalStorageSPtr storage,
                          QXmlStreamWriter   &xml,
                          const QString      &path) const
{
  Snapshot snapshot;

  auto saveOutput = isSegmentationOutput();

  for(auto data : m_data)
  {
    xml.writeStartElement("Data");
    xml.writeAttribute("type",    data->type());
    xml.writeAttribute("bounds",  data->bounds().toString());

    for(int i = 0; i < data->editedRegions().size(); ++i)
    {
      auto region = data->editedRegions()[i];
      xml.writeStartElement("EditedRegion");
      xml.writeAttribute("id",     QString::number(i));
      xml.writeAttribute("bounds", region.toString());
      xml.writeEndElement();
    }
    xml.writeEndElement();

    auto snapshotId = QString::number(id());
    if (saveOutput)
    {
      if (!data->isValid())
      {
        data->fetchData();
      }
      snapshot << data->snapshot(storage, path, snapshotId);
    }
    else
    {
      // Similarly to the output data is the case of the edited regions
      // we need to copy the existing ones
      // Alternatively, until the other implementation is available, we
      // just restore them so they are available on save
      if (!data->isValid())
      {
        data->restoreEditedRegions(storage, path, snapshotId);
      }
      snapshot << data->editedRegionsSnapshot(storage, path, snapshotId);
    }
  }

  return snapshot;
}

//----------------------------------------------------------------------------
Bounds Output::bounds() const
{
  Bounds bounds;

  for(auto data : m_data)
  {
    if (bounds.areValid())
    {
      bounds = boundingBox(bounds, data->bounds());
    } else
    {
      bounds = data->bounds();
    }
  }

  return bounds;
}

//----------------------------------------------------------------------------
void Output::clearEditedRegions()
{
  for(auto data: m_data)
  {
    data->clearEditedRegions();
  }
}

//----------------------------------------------------------------------------
bool Output::isEdited() const
{
  for(auto data: m_data)
  {
    if (data->isEdited()) return true;
  }

  return false;
}

//----------------------------------------------------------------------------
bool Output::isValid() const
{
  if (m_filter == nullptr) return false;

  if (m_id == INVALID_OUTPUT_ID) return false;

  for(auto data : m_data)
  {
    if (!data->isValid()) return false;
  }

  return !m_data.isEmpty();
}

//----------------------------------------------------------------------------
void Output::onDataChanged()
{
  emit modified();
}

//----------------------------------------------------------------------------
void Output::setData(Output::DataSPtr data)
{
  Data::Type type = data->type();

  if (!m_data.contains(type))
  {
    m_data[type] = data->createProxy();
  }

  auto base  = m_data.value(type).get();
  auto proxy = dynamic_cast<DataProxy *>(base);
  proxy->set(data);
  data->setOutput(this);

  // Alternatively we could keep the previous edited regions
  // but at the moment I can't find any scenario where it could be useful
  BoundsList regions;
  regions << data->bounds();
  data->setEditedRegions(regions);

  updateModificationTime();
  emit modified();

  connect(data.get(), SIGNAL(dataChanged()),
          this, SLOT(onDataChanged()));
}

//----------------------------------------------------------------------------
void Output::removeData(const Data::Type& type)
{
  m_data.remove(type);
}

//----------------------------------------------------------------------------
Output::DataSPtr Output::data(const Data::Type& type) const
throw (Unavailable_Output_Data_Exception)
{
  if (m_data.contains(type))
  {
    return m_data.value(type);
  }
  else
  {
    throw Unavailable_Output_Data_Exception();
  }
}

//----------------------------------------------------------------------------
bool Output::hasData(const Data::Type& type) const
{
  return m_data.contains(type);
}

//----------------------------------------------------------------------------
unsigned int Output::numberOfDatas() const
{
  unsigned int result = 0;
  for(auto data : m_data)
  {
    if (data->isValid())
    {
      ++result;
    }
  }

  return result;
}

//----------------------------------------------------------------------------
void Output::update()
{
  for (auto data : m_data)
  {
    if (!data->isValid())
    {
      update(data->type());
    }
  }
}

//----------------------------------------------------------------------------
void Output::update(const Data::Type &type)
{
  auto requestedData = data(type);

  if (!requestedData->isValid())
  {
    BoundsList editedRegions = requestedData->editedRegions();

    if (requestedData->fetchData())
    {
      requestedData->setEditedRegions(editedRegions);
    }
    else
    {
      auto dependencies = requestedData->dependencies();

      for (auto dependencyType : dependencies)
      {
        update(dependencyType);
      }

      auto currentData = data(type);
      if (dependencies.isEmpty() || requestedData == currentData)
      {
        m_filter->update();
      }
    }
  }
}



//----------------------------------------------------------------------------
bool Output::isSegmentationOutput() const
{
  auto analysis = m_filter->analysis();
  if (analysis)
  {
    auto content  = analysis->content();
    auto outEdges = content->outEdges(m_filter, QString::number(m_id));

    for (auto edge : outEdges)
    {
      if (std::dynamic_pointer_cast<Segmentation>(edge.target))
      {
        return true;
      }
    }
  }

  return false;
}
