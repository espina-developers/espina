/*
 *
 * Copyright (C) 2014  Jorge Peña Pastor <jpena@cesvima.upm.es>
 *
 * This file is part of ESPINA.

    ESPINA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ESPINA_METADONA_PROVIDER_H
#define ESPINA_METADONA_PROVIDER_H

#include "Support/EspinaSupport_Export.h"

#include <Coordinator.h>

namespace ESPINA {

  class EspinaSupport_EXPORT Coordinator
  : public Metadona::Coordinator
  {
    virtual Metadona::Id selectEntry(const Metadona::Level& level, std::vector<Metadona::Id> entries) const;

    virtual void createEntry(Metadona::Entry& entry);

    virtual Metadona::Level selectNextLevel(const std::vector<Metadona::Level>& levels);
  };
}

#endif // ESPINA_METADONA_PROVIDER_H
