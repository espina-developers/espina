/*

    Copyright (C) 2014  Jorge Peña Pastor <jpena@cesvima.upm.es>

    This file is part of ESPINA.

    ESPINA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ESPINA_VIEW_3D_H
#define ESPINA_VIEW_3D_H

#include <GUI/View/RenderView.h>

// ESPINA
#include "GUI/Representations/Renderers/Renderer.h"
#include "GUI/Representations/Representation.h"

// VTK
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>

// Qt
#include <QPushButton>

class vtkAbstractWidget;
class QVTKWidget;

//Forward declaration
class QHBoxLayout;
class QPushButton;
class QVBoxLayout;
class QHBoxLayout;
class QScrollBar;

namespace ESPINA
{
  class EspinaGUI_EXPORT View3D
  : public RenderView
  {
    Q_OBJECT
  public:
    /** \brief View3D class constructor.
     * \param[in] showCrosshairPlaneSelectors, true to show three aditional
     *            scrollbars in the borders of the view to manipulate the
     *            crosshair point, false otherwise.
     * \param[in] parent, raw pointer of the QWidget parent of this one.
     *
     */
    explicit View3D(bool showCrosshairPlaneSelectors = false,
                    QWidget* parent = nullptr);

    /** \brief View3D class virtual destructor.
     *
     */
    virtual ~View3D();

    /** \brief Sets the focal point of the camera in the given point.
     * \param[in] center, focal point.
     *
     */
    void setCameraFocus(const NmVector3& center);

    /** \brief Implements RenderView::reset().
     *
     */
    virtual void reset();

    /** \brief Implements RenderView::resetCamera().
     *
     */
    virtual void resetCamera();

    /** \brief Implements RenderView::centerViewOn().
     *
     */
    virtual void centerViewOn(const NmVector3& center, bool force = false);

    /** \brief Overrides RenderView::addWidget().
     *
     */
    virtual void addWidget(EspinaWidgetSPtr widget) override;

    /** \brief Overrides RenderView::removeWidget().
     *
     */
    virtual void removeWidget(EspinaWidgetSPtr widget) override;

    /** \brief Implements RenderView::previewBounds().
     *
     */
    virtual Bounds previewBounds(bool cropToSceneBounds = true) const;

    /** \brief Overrides RenderView::add(channel).
     *
     */
    virtual void add(ChannelAdapterPtr channel) override;

    /** \brief Overrides RenderView::add(segmentation)
     *
     */
    virtual void add(SegmentationAdapterPtr seg) override
    { RenderView::add(seg); }

    /** \brief Overrides RenderView::remove(channel).
     *
     */
    virtual void remove(ChannelAdapterPtr channel) override;

    /** \brief Overrides RenderView::remove(segmentation).
     *
     */
    virtual void remove(SegmentationAdapterPtr seg) override
    { RenderView::remove(seg); }

    /** \brief Overrides RenderView::updateRepresentation(channel).
     *
     */
    virtual bool updateRepresentation(ChannelAdapterPtr channel, bool render = true) override;

    /** \brief Overrides RenderView::updateRepresentation(segmentation).
     *
     */
    virtual bool updateRepresentation(SegmentationAdapterPtr seg, bool render = true);

    /** \brief Modifies the position of a specified plane of the crosshair to the given position.
     * \param[in] plane, crosshair plane to move.
     * \param[in] position, new position.
     *
     */
    void changePlanePosition(Plane plane, Nm position);

    /** \brief Implements RenderView::addRendererControls().
     *
     */
    void addRendererControls(RendererSPtr renderer);

    /** \brief Implements RenderView::removeRendererControls().
     *
     */
    void removeRendererControls(const QString name);

    /** \brief Overrides QObject::eventFilter().
     *
     */
    virtual bool eventFilter(QObject* caller, QEvent* e) override;

    /** \brief Implements RenderView::cloneRepresentation().
     *
     */
    virtual RepresentationSPtr cloneRepresentation(ESPINA::ViewItemAdapterPtr item, ESPINA::Representation::Type representation);

    /** \brief Implements RenderView::setRenderers().
     *
     */
    void setRenderers(RendererSList renderers);

    /** \brief Implements RenderView::activateRender().
     *
     */
    void activateRender(const QString &rendererName);

    /** \brief Implements RenderView::deactivateRender().
     *
     */
    void deactivateRender(const QString &rendererName);

    /** \brief Implements RenderView::setVisualState().
     *
     */
    virtual void setVisualState(struct RenderView::VisualState);

    /** \brief Implements RenderView::visualState().
     *
     */
    virtual struct RenderView::VisualState visualState();

    /** \brief Implements RenderView::select(flags, SCREEN x, SCREEN y)
     *
     */
    Selector::Selection select(const Selector::SelectionFlags flags, const int x, const int y, bool multiselection = true) const;

  public slots:
  /** \brief Implements RenderView::updateView().
		 *
		 */
    virtual void updateView();

  signals:
    void centerChanged(NmVector3);

  protected:
    /** \brief Selects items under the given coordinates.
     * \param[in] x, x display coordinate.
     * \param[in] y, y display coordinate.
     * \param[in] append, if true returns all the items if false returns only the first picked (if any).
     *
     */
    void selectPickedItems(int x, int y, bool append);

    /** \brief Implements RenderView::updateChannelsOpacity().
     *
     */
    virtual void updateChannelsOpacity()
    {}

  protected slots:
		/** \brief Updates the view then a crosshair scroll bar changes value.
		 * \param[in] value, new scrollbar value.
		 *
		 */
  	void scrollBarMoved(int value);

    /** \brief Exports the scene meshes to an external format and saves the result to disk.
     *
     */
  	void exportScene();

    /** \brief Takes an 2D image of the current view and saves it to disk.
     *
     */
    void onTakeSnapshot();

    /** \brief Updates the state of the renderers controls.
     *
     */
    void updateRenderersControls();

  private:
    /** \brief Helper method to setup the UI.
     *
     */
    void setupUI();

    /** \brief Helper method to build the UI controls.
     *
     */
    void buildControls();

    /** \brief Helper method to update the limit of the crosshair scrollbars.
     *
     */
    void updateScrollBarsLimits();

  private:
    // GUI
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_controlLayout;
    QPushButton *m_snapshot;
    QPushButton *m_export;
    QPushButton *m_zoom;
    QPushButton *m_renderConfig;

    // GUI elements only visible in Segmentation Information dialog
    QHBoxLayout *m_additionalGUI;
    QScrollBar  *m_axialScrollBar;
    QScrollBar  *m_coronalScrollBar;
    QScrollBar  *m_sagittalScrollBar;

    bool m_showCrosshairPlaneSelectors;

    NmVector3 m_center;

    unsigned int m_numEnabledRenderers;
  };

  /** \brief Returns true if the view is a 3D view.
   * \param[in] view, RenderView raw pointer.
   *
   */
  inline bool isView3D(RenderView *view)
  { return dynamic_cast<View3D *>(view) != nullptr; }

  /** \brief Returns the 3D view raw pointer given a RenderView raw pointer.
   * \param[in] view, RenderView raw pointer.
   *
   */
  inline View3D * view3D_cast(RenderView* view)
  { return dynamic_cast<View3D *>(view); }

} // namespace ESPINA

#endif // ESPINA_VIEW_3D_H
