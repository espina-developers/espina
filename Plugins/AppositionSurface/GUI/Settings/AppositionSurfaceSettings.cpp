/*
 * AppositionSurfaceSettings.cpp
 *
 *  Created on: Jan 16, 2013
 *      Author: Felix de las Pozas Alvarez
 */

// ESPINA
#include "AppositionSurfaceSettings.h"
#include <Support/Settings/EspinaSettings.h>

// Qt
#include <QSettings>
#include <QColorDialog>

namespace ESPINA
{
  
  //-----------------------------------------------------------------------------
  AppositionSurfaceSettings::AppositionSurfaceSettings()
  {
    setupUi(this);

    ESPINA_SETTINGS(settings);
    settings.beginGroup("Apposition Surface");

    if (settings.contains("Automatic Computation For Synapses"))
      m_automaticComputation = settings.value("Automatic Computation For Synapses").toBool();
    else
    {
      m_automaticComputation = false;
      settings.setValue("Automatic Computation For Synapses", m_automaticComputation);
    }
    settings.sync();

    m_modified = false;
    defaultComputation->setChecked(m_automaticComputation);
    connect(defaultComputation, SIGNAL(stateChanged(int)),
            this, SLOT(changeDefaultComputation(int)));
  }

  //-----------------------------------------------------------------------------
  void AppositionSurfaceSettings::changeDefaultComputation(int value)
  {
    m_automaticComputation = (Qt::Checked == value ? true : false);
    m_modified = true;
  }

  //-----------------------------------------------------------------------------
  void AppositionSurfaceSettings::acceptChanges()
  {
    if (!m_modified)
      return;

    ESPINA_SETTINGS(settings);
    settings.beginGroup("Apposition Surface");
    settings.setValue("Automatic Computation For Synapses", m_automaticComputation);
    settings.sync();
  }

  //-----------------------------------------------------------------------------
  void AppositionSurfaceSettings::rejectChanges()
  {
  }

  //-----------------------------------------------------------------------------
  bool AppositionSurfaceSettings::modified() const
  {
    return m_modified;
  }

  //-----------------------------------------------------------------------------
  SettingsPanelPtr AppositionSurfaceSettings::clone()
  {
    return new AppositionSurfaceSettings();
  }

} /* namespace ESPINA */
