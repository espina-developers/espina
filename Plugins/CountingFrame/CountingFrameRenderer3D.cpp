/*

    Copyright (C) 2014  Jorge Peña Pastor <jpena@cesvima.upm.es>

    This file is part of ESPINA.

    ESPINA is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Plugin
#include "CountingFrameRenderer3D.h"
#include "CountingFrames/CountingFrame.h"
#include <GUI/View/View3D.h>

// VTK
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderWindow.h>
#include <vtkAbstractWidget.h>
#include <vtkWidgetRepresentation.h>

using namespace ESPINA;
using namespace ESPINA::CF;

//-----------------------------------------------------------------------------
CountingFrameRenderer3D::CountingFrameRenderer3D(CountingFrameManager& cfManager)
: m_cfManager(cfManager)
{
  connect(&m_cfManager, SIGNAL(countingFrameCreated(CountingFrame*)),
          this, SLOT(onCountingFrameCreated(CountingFrame*)));
  connect(&m_cfManager, SIGNAL(countingFrameDeleted(CountingFrame*)),
          this, SLOT(onCountingFrameDeleted(CountingFrame*)));

  m_enable = false;
}

//-----------------------------------------------------------------------------
CountingFrameRenderer3D::~CountingFrameRenderer3D()
{
  for(auto cf: m_widgets.keys())
    onCountingFrameDeleted(cf);
}

//-----------------------------------------------------------------------------
void CountingFrameRenderer3D::hide()
{
  if (!m_enable)
    return;

  bool updated = false;
  for(auto cf : m_widgets.keys())
  {
    bool visible = cf->isVisible();

    if (visible)
      m_widgets[cf]->SetEnabled(false);

    updated |= visible;
  }

  if (updated)
    emit renderRequested();
}

//-----------------------------------------------------------------------------
void CountingFrameRenderer3D::show()
{
  if (m_enable)
    return;

  bool updated = false;

  for(auto cf : m_widgets.keys())
  {
    bool visible = cf->isVisible();

    if (visible)
      m_widgets[cf]->SetEnabled(true);

    updated |= visible;
  }

  if (updated)
    emit renderRequested();
}

//-----------------------------------------------------------------------------
unsigned int CountingFrameRenderer3D::numberOfvtkActors() const
{
  return m_enable? m_cfManager.countingFrames().size() * 2 : 0; // m_boundingRegion & m_representation vtkPolyDatas...
}

//-----------------------------------------------------------------------------
void CountingFrameRenderer3D::onCountingFrameCreated(CountingFrame *cf)
{
  if (!m_view || m_widgets.keys().contains(cf))
    return;

  cf->registerView(m_view);
  auto widget = dynamic_cast<vtkCountingFrame3DWidget *>(cf->getWidget(m_view));
  widget->SetEnabled(m_enable);
  m_widgets.insert(cf, widget);

  if (m_enable)
    emit renderRequested();

  connect(cf, SIGNAL(changedVisibility()), this, SLOT(visibilityChanged()), Qt::QueuedConnection);
}

//-----------------------------------------------------------------------------
void CountingFrameRenderer3D::onCountingFrameDeleted(CountingFrame *cf)
{
  if (!m_view || !m_widgets.keys().contains(cf))
    return;

  m_widgets.remove(cf);

  if (m_enable)
    emit renderRequested();
}

//-----------------------------------------------------------------------------
RendererSPtr CountingFrameRenderer3D::clone() const
{
  return RendererSPtr(new CountingFrameRenderer3D(m_cfManager));
}

//-----------------------------------------------------------------------------
void CountingFrameRenderer3D::visibilityChanged()
{
  if (!m_enable)
    return;

  auto cf = dynamic_cast<CountingFrame *>(sender());
  if (!m_widgets.keys().contains(cf) || !m_enable)
    return;

  bool visible = cf->isVisible();
  m_widgets[cf]->SetEnabled(visible);
  emit renderRequested();
}

//-----------------------------------------------------------------------------
void CountingFrameRenderer3D::setView(RenderView *view)
{
  m_view = view;

  auto existingCFs = m_cfManager.countingFrames();
  for(auto cf: existingCFs)
    onCountingFrameCreated(cf);
}
