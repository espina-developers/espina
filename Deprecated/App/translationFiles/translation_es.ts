<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES" sourcelanguage="en">
<context>
    <name>CountingRegion</name>
    <message>
        <source>Counting Brick</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CountingRegionPanel</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left Margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right Margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upper Slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Margin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lower Slice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bottom Margin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CrosshairRenderer</name>
    <message>
        <source>Show planes</source>
        <translation>Mostrar planos</translation>
    </message>
</context>
<context>
    <name>EspinaMainWindow</name>
    <message>
        <source>Type of new segmentation</source>
        <translation>Tipo de la nueva segmentación</translation>
    </message>
    <message>
        <source>Open File:</source>
        <translation>Abrir fichero:</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <source>Save Trace</source>
        <translation>Salvar traza</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <source>Open - ParaView mode</source>
        <translation>Abrir - Modo ParaView</translation>
    </message>
    <message>
        <source>Save - ParaView mode</source>
        <translation>Salvar - Modo ParaView</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeshRenderer</name>
    <message>
        <source>Mesh render</source>
        <translation>Renderizado de maya</translation>
    </message>
</context>
<context>
    <name>SampleEditor</name>
    <message>
        <source>Sample Editor</source>
        <translation>Editor de muestra</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>nm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>um</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeedGrowRegionSegmentation</name>
    <message>
        <source>Add synapse (Ctrl +). Exact Pixel</source>
        <translation>Pixel Exacto</translation>
    </message>
    <message>
        <source>Add synapse (Ctrl +). Best Pixel</source>
        <translation>Mejor Pixel</translation>
    </message>
    <message>
        <source>Determine the size of color value range for a given pixel</source>
        <translation>Determina el rango de valores de color para un pixel</translation>
    </message>
    <message>
        <source>Pixel selector</source>
        <translation>Selector de pixel</translation>
    </message>
    <message>
        <source>Region Threshold</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SeedGrowSegmentation</name>
    <message>
        <source>Add synapse (Ctrl +). Exact Pixel</source>
        <translation>Pixel Exacto</translation>
    </message>
    <message>
        <source>Add synapse (Ctrl +). Best Pixel</source>
        <translation>Mejor Pixel</translation>
    </message>
    <message>
        <source>Threshold</source>
        <translation>Umbral</translation>
    </message>
    <message>
        <source>Determine the size of color value range for a given pixel</source>
        <translation>Determina el rango de valores de color para un pixel</translation>
    </message>
    <message>
        <source>Pixel selector</source>
        <translation>Selector de pixel</translation>
    </message>
</context>
<context>
    <name>SegmentationExplorer</name>
    <message>
        <source>Segmentation Exporer</source>
        <translation>Explorador de segmentaciones</translation>
    </message>
</context>
<context>
    <name>SelectedPixelList</name>
    <message>
        <source>File to save selected points</source>
        <translation>Fichero para salvar los puntos seleccionados</translation>
    </message>
    <message>
        <source>Selected pixel list</source>
        <translation>Lista de selección de píxeles</translation>
    </message>
</context>
<context>
    <name>VolumeOfInterest</name>
    <message>
        <source>Volume Of Interest</source>
        <translation>Volumen de Interés</translation>
    </message>
</context>
<context>
    <name>VolumetricRenderer</name>
    <message>
        <source>Volumetric render</source>
        <translation>Renderizado volumétrico</translation>
    </message>
</context>
<context>
    <name>pqClientMainWindow</name>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Vista</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>&amp;Herramientas</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <source>Add subtaxonomy node</source>
        <translation>Añadir un nodo taxonómico hijo</translation>
    </message>
    <message>
        <source>Remove taxonomy node</source>
        <translation>Eliminar nodo taxonómico</translation>
    </message>
    <message>
        <source>Samples</source>
        <translation>Muestras</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Deshacer</translation>
    </message>
    <message>
        <source>Undo last action</source>
        <translation>Deshacer última acción</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Rehacer</translation>
    </message>
    <message>
        <source>Re-do previously undo action</source>
        <translation>Rehacer último deshacer</translation>
    </message>
    <message>
        <source>Visibility</source>
        <translation>Visibilidad</translation>
    </message>
    <message>
        <source>Turn Visibility On/Off</source>
        <translation>Mostrar/Ocultar objectos</translation>
    </message>
    <message>
        <source>Space</source>
        <translation>Espacio</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Statistics Inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pipeline Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X-Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y-Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ESPINA Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Segmentations</source>
        <translation>Segmentaciones</translation>
    </message>
    <message>
        <source>Group by:</source>
        <translation>Agrupar por:</translation>
    </message>
    <message>
        <source>Taxonomies</source>
        <translation>Taxonomías</translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete selected segmentation</source>
        <translation>Elimnar segmentacion seleccionada</translation>
    </message>
    <message>
        <source>Change taxomy node color</source>
        <translation>Cambiar el color de la taxonomía</translation>
    </message>
    <message>
        <source>Add taxonomy node</source>
        <translation>Añadir elemento taxonómico</translation>
    </message>
</context>
</TS>
